submitBtn.onclick = function() {
    switch (profil.value) {
        case "1":
            browser.storage.local.set({
                profile1: {
                    kunde: kunde.value,
                    arbeitsOrt: arbeitsOrt.value,
                    leistungsart: leistungsart.value,
                    kostenart: kostenart.value,
                    beginnArbeit: beginnArbeit.value,
                    endeArbeit: endeArbeit.value,
                    endeArbeitFreitag: endeArbeitFreitag.value,
                    pause: pause.value,
                    pauseFreitag: pauseFreitag.value
                }
            })
            break;

        case "2":
            browser.storage.local.set({
                profile2: {
                    kunde: kunde.value,
                    arbeitsOrt: arbeitsOrt.value,
                    leistungsart: leistungsart.value,
                    kostenart: kostenart.value,
                    beginnArbeit: beginnArbeit.value,
                    endeArbeit: endeArbeit.value,
                    endeArbeitFreitag: endeArbeitFreitag.value,
                    pause: pause.value,
                    pauseFreitag: pauseFreitag.value
                }
            })
            break;
        case "3":
            browser.storage.local.set({
                profile3: {
                    kunde: kunde.value,
                    arbeitsOrt: arbeitsOrt.value,
                    leistungsart: leistungsart.value,
                    kostenart: kostenart.value,
                    beginnArbeit: beginnArbeit.value,
                    endeArbeit: endeArbeit.value,
                    endeArbeitFreitag: endeArbeitFreitag.value,
                    pause: pause.value,
                    pauseFreitag: pauseFreitag.value
                }
            })
            break;
    }
    alert("Gespeichert!")
}

window.onload = () => {
    browser.storage.local.get({
        profile1: {
            kunde: "201109",
            arbeitsOrt: "Siegen",
            leistungsart: 1,
            kostenart: 15,
            beginnArbeit: "07:45",
            endeArbeit: "16:45",
            endeArbeitFreitag: "13:00",
            pause: "1.25",
            pauseFreitag: "0.25"
        },
        profile2: {
            kunde: "201109",
            arbeitsOrt: "Homeoffice",
            leistungsart: 1,
            kostenart: 15,
            beginnArbeit: "07:45",
            endeArbeit: "16:45",
            endeArbeitFreitag: "13:00",
            pause: "1.25",
            pauseFreitag: "0.25"
        },
        profile3: {
            kunde: "201109",
            arbeitsOrt: "Berufskolleg Technik Siegen",
            leistungsart: 1,
            kostenart: 24,
            beginnArbeit: "07:45",
            endeArbeit: "16:45",
            endeArbeitFreitag: "13:00",
            pause: "1.25",
            pauseFreitag: "0.25"
        }
    }).then((item) => {
        kunde.value = item.profile1.kunde
        arbeitsOrt.value = item.profile1.arbeitsOrt
        leistungsart.value = item.profile1.leistungsart
        kostenart.value = item.profile1.kostenart
        beginnArbeit.value = item.profile1.beginnArbeit
        endeArbeit.value = item.profile1.endeArbeit
        endeArbeitFreitag.value = item.profile1.endeArbeitFreitag
        pause.value = item.profile1.pause
        pauseFreitag.value = item.profile1.pauseFreitag
    })
}

profil.onchange = function (event) {
    switch (event.target.value) {
        case "1":
            browser.storage.local.get({
                profile1: {
                    kunde: "201109",
                    arbeitsOrt: "Siegen",
                    leistungsart: 1,
                    kostenart: 15,
                    beginnArbeit: "07:45",
                    endeArbeit: "16:45",
                    endeArbeitFreitag: "13:00",
                    pause: "1.25",
                    pauseFreitag: "0.25"
                },
            }).then((item) => {
                kunde.value = item.profile1.kunde
                arbeitsOrt.value = item.profile1.arbeitsOrt
                leistungsart.value = item.profile1.leistungsart
                kostenart.value = item.profile1.kostenart
                beginnArbeit.value = item.profile1.beginnArbeit
                endeArbeit.value = item.profile1.endeArbeit
                endeArbeitFreitag.value = item.profile1.endeArbeitFreitag
                pause.value = item.profile1.pause
                pauseFreitag.value = item.profile1.pauseFreitag
            })
            break;
        case "2":
            browser.storage.local.get({
                profile2: {
                    kunde: "201109",
                    arbeitsOrt: "Homeoffice",
                    leistungsart: 1,
                    kostenart: 15,
                    beginnArbeit: "07:45",
                    endeArbeit: "16:45",
                    endeArbeitFreitag: "13:00",
                    pause: "1.25",
                    pauseFreitag: "0.25"
                },
            }).then((item) => {
                kunde.value = item.profile2.kunde
                arbeitsOrt.value = item.profile2.arbeitsOrt
                leistungsart.value = item.profile2.leistungsart
                kostenart.value = item.profile2.kostenart
                beginnArbeit.value = item.profile2.beginnArbeit
                endeArbeit.value = item.profile2.endeArbeit
                endeArbeitFreitag.value = item.profile2.endeArbeitFreitag
                pause.value = item.profile2.pause
                pauseFreitag.value = item.profile2.pauseFreitag
            })
            break;
        case "3":
            browser.storage.local.get({
                profile3: {
                    kunde: "201109",
                    arbeitsOrt: "Berufskolleg Technik Siegen",
                    leistungsart: 1,
                    kostenart: 24,
                    beginnArbeit: "07:45",
                    endeArbeit: "16:45",
                    endeArbeitFreitag: "13:00",
                    pause: "1.25",
                    pauseFreitag: "0.25"
                },
            }).then((item) => {
                kunde.value = item.profile3.kunde
                arbeitsOrt.value = item.profile3.arbeitsOrt
                leistungsart.value = item.profile3.leistungsart
                kostenart.value = item.profile3.kostenart
                beginnArbeit.value = item.profile3.beginnArbeit
                endeArbeit.value = item.profile3.endeArbeit
                endeArbeitFreitag.value = item.profile3.endeArbeitFreitag
                pause.value = item.profile3.pause
                pauseFreitag.value = item.profile3.pauseFreitag
            })
            break;
    }

}