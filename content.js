console.log("Content Script geladen")

let counter = 0;
let selProfile = {};

browser.runtime.onMessage.addListener(function(request, sender, sendResponse) {
    counter++;
    setTimeout(async function() {
        switch(counter) {
            case 1:
                await browser.storage.local.get({
                    profile1: {
                        kunde: "201109",
                        arbeitsOrt: "Siegen",
                        leistungsart: 1,
                        kostenart: 15,
                        beginnArbeit: "07:45",
                        endeArbeit: "16:45",
                        endeArbeitFreitag: "13:00",
                        pause: "1.25",
                        pauseFreitag: "0.25"
                    }
                    
                }).then((item) => {
                    selProfile = item.profile1;
                })
                break;
            case 2:
                await browser.storage.local.get({
                    profile2: {
                        kunde: "201109",
                        arbeitsOrt: "Homeoffice",
                        leistungsart: 1,
                        kostenart: 15,
                        beginnArbeit: "07:45",
                        endeArbeit: "16:45",
                        endeArbeitFreitag: "13:00",
                        pause: "1.25",
                        pauseFreitag: "0.25"
                    }
                    
                }).then((item) => {
                    selProfile = item.profile2;
                })
                break;
            case 3:
                await browser.storage.local.get({
                    profile3: {
                        kunde: "201109",
                        arbeitsOrt: "Berufskolleg Technik Siegen",
                        leistungsart: 1,
                        kostenart: 24,
                        beginnArbeit: "07:45",
                        endeArbeit: "16:45",
                        endeArbeitFreitag: "13:00",
                        pause: "1.25",
                        pauseFreitag: "0.25"
                    }
                    
                }).then((item) => {
                    selProfile = item.profile3;
                })
                break;
        }

        if(counter != 0) {

            console.log("Fülle Felder mit Profil " + counter, selProfile)
            
            // console.log("Profil: ", selProfile)
            // Kunde
            document.getElementById("phc_ucArbBeri_luKunde").value = selProfile.kunde
            
            // Arbeitsort
            document.getElementById("phc_ucArbBeri_tbArbeitsOrt").value = selProfile.arbeitsOrt
            
            let today = new Date();
            let aktTag = today.getDate();
            let aktMonat = today.getMonth()+1;
            if(aktTag.toString().length < 2) aktTag = "0" + aktTag;
            if(aktMonat.toString().length < 2) aktMonat = "0" + JSON.stringify(today.getMonth()+1); 
            let todayFormatted = aktTag + "." + aktMonat + "." + today.getFullYear();
            
            // Datum
            document.getElementById("phc_ucArbBeri_dpDate").value = todayFormatted
            
            // Leistungsart
            document.getElementById("phc_ucArbBeri_ddWageTypeKd").value = selProfile.leistungsart
            
            // Kostenart
            document.getElementById("phc_ucArbBeri_ddWorkingProcessDd").value = selProfile.kostenart
            
            // Beginn Arbeit
            document.getElementById("phc_ucArbBeri_tbBeginWork").value = selProfile.beginnArbeit
            

            // Ende Arbeit + Ende Fahrt + Pause
            if (today.getDay() === 5) {
                // Freitags
                document.getElementById("phc_ucArbBeri_tbEndWork").value = selProfile.endeArbeitFreitag
                document.getElementById("phc_ucArbBeri_tbEndDrive").value = selProfile.endeArbeitFreitag
                document.getElementById("phc_ucArbBeri_mbBReak").value = selProfile.pauseFreitag
            } else {
                document.getElementById("phc_ucArbBeri_tbEndWork").value = selProfile.endeArbeit
                document.getElementById("phc_ucArbBeri_tbEndDrive").value = selProfile.endeArbeit
                document.getElementById("phc_ucArbBeri_mbBReak").value = selProfile.pause
            }
            counter = 0;
        }
    }, 500)
});


/*function(request, sender, sendResponse) {
    browser.storage.local.get({
        kunde: "201109",
        arbeitsOrt: "Siegen",
        leistungsart: 1,
        kostenart: 15,
        beginnArbeit: "07:45",
        endeArbeit: "16:45",
        endeArbeitFreitag: "13:00",
        pause: "1.25",
        pauseFreitag: "0.25"
    }).then((item) => {
        console.log("Fülle Felder...")
        
        // Kunde
        document.getElementById("phc_ucArbBeri_luKunde").value = item.kunde
        
        // Arbeitsort
        document.getElementById("phc_ucArbBeri_tbArbeitsOrt").value = item.arbeitsOrt
        
        let today = new Date()
        let todayFormatted = today.getDate() + ".02." + today.getFullYear()
        
        // Datum
        document.getElementById("phc_ucArbBeri_dpDate").value = todayFormatted
        
        // Leistungsart
        document.getElementById("phc_ucArbBeri_ddWageTypeKd").value = item.leistungsart
        
        // Kostenart
        document.getElementById("phc_ucArbBeri_ddWorkingProcessDd").value = item.kostenart
        
        // Beginn Arbeit
        document.getElementById("phc_ucArbBeri_tbBeginWork").value = item.beginnArbeit
        

        // Ende Arbeit + Ende Fahrt + Pause
        if (today.getDay() === 5) {
            // Freitags
            document.getElementById("phc_ucArbBeri_tbEndWork").value = item.endeArbeitFreitag
            document.getElementById("phc_ucArbBeri_tbEndDrive").value = item.endeArbeitFreitag
            document.getElementById("phc_ucArbBeri_mbBReak").value = item.pauseFreitag
        } else {
            document.getElementById("phc_ucArbBeri_tbEndWork").value = item.endeArbeit
            document.getElementById("phc_ucArbBeri_tbEndDrive").value = item.endeArbeit
            document.getElementById("phc_ucArbBeri_mbBReak").value = item.pause
        }
    })
});
*/